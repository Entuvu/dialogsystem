﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Dialog Data", menuName = "Dialog Tutorial/Data/Create Dialog Data")]
public class DialogDataTemplate : ScriptableObject
{
    [SerializeField] private DialogData data;
    public DialogData Data { get { return data; } }
}
