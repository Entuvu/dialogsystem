﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogData
{
    [SerializeField] private List<string> dialogTextList;
    [SerializeField] private bool destroyOnUse;

    public List<string> DialogTextList { get { return dialogTextList; } }
    public bool DestroyOnUse { get { return destroyOnUse;  } }
}
