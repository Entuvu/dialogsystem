﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour
{
    public DialogDataTemplate dialogData;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            DialogSystem.Instance.StartDialog(dialogData.Data);
            if(dialogData.Data.DestroyOnUse == true)
            {
                Destroy(gameObject);
            }
        }
    }
}
