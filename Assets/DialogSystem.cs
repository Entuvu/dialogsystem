﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogSystem : MonoBehaviour
{
    public static DialogSystem Instance;

    [SerializeField] private GameObject dialogPanel;
    [SerializeField] private TMPro.TextMeshProUGUI dialogText;
    [SerializeField] private float standardTypingSpeed = 1f;
    [Range(.01f, .1f)] [SerializeField] private float typingSpeedMultiplier = 0.1f;

    private float currentTypingSpeed = 1f;

    [SerializeField] private List<string> textToType = new List<string>();
    [SerializeField] private bool typing = false;
    private float _timer = 0f;

    private void Awake()
    {
        // Singleton //
        if(Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        dialogText.text = "";
    }

    private void Update()
    {
        if (typing)
        {
            PressSpaceToClose();
            TypeText();
        }
    }

    private void PressSpaceToClose()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (string.IsNullOrEmpty(textToType[0]))
            {
                NextWord();
                if (textToType.Count < 1)
                {
                    dialogPanel.SetActive(false);
                    typing = false;
                }
            }
            else
            {
                SpeedUp();
            }
        }
    }

    private void NextWord()
    {
        dialogText.text = "";
        textToType.RemoveAt(0);
        currentTypingSpeed = standardTypingSpeed; //reset the speed/
    }

    private void SpeedUp()
    {
        currentTypingSpeed = standardTypingSpeed * typingSpeedMultiplier;
    }

    private void TypeText()
    {
        if (textToType.Count > 0)
        {
            if (!string.IsNullOrEmpty(textToType[0]))
            {
                _timer += Time.deltaTime;
                if (_timer >= currentTypingSpeed)
                {
                    _timer = 0f;
                    ShowNextLetter();
                }
            }
        }
        
    }

    private void ShowNextLetter()
    { 
        dialogText.text = dialogText.text + textToType[0][0].ToString();
        textToType[0] = textToType[0].Remove(0, 1);
    }

    public void StartDialog(DialogData data)
    {
        dialogPanel.SetActive(true);
        this.dialogText.text = ""; // Clear out text if it has text already //
        textToType = data.DialogTextList.Clone();
        typing = true;
    }
}
